from ..watchdog.edges import edges as watchdog_edges
from ..masterswitch.edges import edges as masterswitch_edges
from ..isolation.edges import edges as isolation_edges

edges = []
edges += watchdog_edges
edges += masterswitch_edges
edges += isolation_edges

edges += [
        # state A       -->     state B 
        ('READY', 'LOAD_CART_BIAS_FOR_ISOLATION'),
        ('DEISOLATING', 'READY'),
        ('INIT','READY'),
        ]
