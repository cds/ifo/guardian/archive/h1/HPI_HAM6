from .. import const as top_const

DAMPING_CONSTANTS_ALL = dict(
        HAM = dict(
            ALL_DOF = top_const.DOF_LIST,
            GAIN = 1,
            RAMP_UP_TIME = 3,  # seconds
            RAMP_DOWN_TIME = 1,  # seconds
            FILTER_NAMES = ['FM1'],
            ONLY_ON_BUTTONS = ['INPUT', 'OUTPUT', 'DECIMATE'],
        ),
        BSC_ST1 = dict(
            ALL_DOF = top_const.DOF_LIST,
            GAIN = 1,
            RAMP_UP_TIME = 3,  # seconds
            RAMP_DOWN_TIME = 1,  # seconds
            FILTER_NAMES = ['FM1'],
            ONLY_ON_BUTTONS = ['INPUT', 'OUTPUT', 'DECIMATE'],
        ),
        BSC_ST2 = dict(
            ALL_DOF = top_const.DOF_LIST,
            GAIN = 1,
            RAMP_UP_TIME = 3,  # seconds
            RAMP_DOWN_TIME = 1,  # seconds
            FILTER_NAMES = ['FM1', 'FM2', 'FM3'],
            ONLY_ON_BUTTONS = ['INPUT', 'OUTPUT', 'DECIMATE'],
        ),
        HPI = dict(),
    )

DAMPING_CONSTANTS = DAMPING_CONSTANTS_ALL[top_const.CHAMBER_TYPE]
